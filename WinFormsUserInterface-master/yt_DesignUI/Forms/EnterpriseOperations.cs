﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using yt_DesignUI.Models;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Rebar;

namespace yt_DesignUI.Forms
{
    public partial class EnterpriseOperations : Design
    {
        private Timer timer;
        private Employee currentWorker;
        private Enterprise currentEnterprise;
        public EnterpriseOperations(Employee worker, Enterprise enterprise)
        {
            InitializeComponent();
            timer = new Timer();
            timer.Interval = 1000;
            timer.Tick += Timer_Tick;
            timer.Start();
            this.currentWorker = worker;
            this.currentEnterprise = enterprise;
            renderLog();
        }
        private void Timer_Tick(object sender, EventArgs e)
        {
            label3.Text = DateTime.Now.ToString("yyyy-MM-dd") + " ";
            label3.Text += DateTime.Now.ToString("HH:mm:ss");
        }

        private void EnterpriseOperations_Load(object sender, EventArgs e)
        {
            label4.Text += currentEnterprise.ContactNumber.ToString();
        }
        public void renderLog()
        {
            
            ChartArea chartArea1 = new ChartArea();
           // chart1.ChartAreas.Add(chartArea1);

            // Создаем новую серию
            Series series1 = new Series();
            chart1.Series.Add(series1);
            series1.ChartType = SeriesChartType.Line;
            currentEnterprise.addToWorkers(currentWorker);
            currentEnterprise.addToWorkers(currentWorker);
            currentEnterprise.addToWorkers(currentWorker);
           List <Employee>  workers = currentEnterprise.returnEmployee();
            float total = 0;
            listBox1.Items.Add($"     this month: {DateTime.Now.ToString("MMMM")}");
            foreach (Employee employee in workers)
            {
                listBox1.Items.Add($"{employee.Name} - {employee.Rate*employee.WorkHours} \n");
                total += employee.Rate * employee.WorkHours;
                series1.Points.AddXY(employee.WorkHours,employee.Rate);
            }
            listBox1.Items.Add($"Total: {total}");



            label5.Text = $"Current entrpise has {currentEnterprise.returnEmployee().Count()} workers;";
            int supervisorCount = currentEnterprise.returnEmployee().Count(e => e is Supervisor);
            label6.Text = $"  {supervisorCount} admins;";
            label7.Text = $"salaries:  {total};";
            label8.Text = $"profit:  {total*0.5};";

        }
       

        private void yt_Button1_Click(object sender, EventArgs e)
        {
            AddNewEnterprise addNew =new  AddNewEnterprise(currentWorker,currentEnterprise);
            this.Hide();
            addNew.ShowDialog();
            this.Show();

        }

        private void yt_Button3_Click(object sender, EventArgs e)
        {
            EnterpriseRemoving remove = new EnterpriseRemoving(currentWorker, currentEnterprise);
            this.Hide();
            remove.ShowDialog();
            this.Show();
        }

       

        private void yt_Button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void yt_Button4_Click(object sender, EventArgs e)
        {
            int parsedValue;
            if (textBox1.Text == "")             
            {
                ListManager.changeIndex(0);
                MessageBox.Show("Index changed on 0");
            }else
            if (int.TryParse(textBox1.Text, out parsedValue))
            {
                if (ListManager.getEnterprise().Count >= int.Parse(textBox1.Text))
                {
                    ListManager.changeIndex(int.Parse(textBox1.Text));
                    MessageBox.Show($"Index changed on {int.Parse(textBox1.Text)}");

                }
            }else
                MessageBox.Show($"Input number to change!");

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text.Length > ListManager.getEnterprise().Count)
                textBox1.Text = textBox1.Text[0].ToString();

            if (!int.TryParse(textBox1.Text, out int number))
            {
                
                textBox1.Text = "";
            }

        }
    }
}
