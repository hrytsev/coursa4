﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Linq;

namespace yt_DesignUI.Models
{
    internal static class ListManager
    {
        private static List<Employee> employeeList = new List<Employee>();      
        private static List<Enterprise> enterpriseList = new List<Enterprise>();
        private static int currentEnterpriseIndex=0;
       
        public static void changeIndex(int index)
        {
            currentEnterpriseIndex = index;
        }
        public static int getIndex()
        {
            return currentEnterpriseIndex;
        }
        public static List<Employee> getEmployeers()
        {
            return employeeList;
        }
       
        public static List<Enterprise> getEnterprise()
        {
            return enterpriseList;
        }

        public static void addNewEmployee(Employee employee)
        {
            employeeList.Add(employee);
        }
        public static void addNewEnterprise(Enterprise enterprise)
        {
            enterpriseList.Add(enterprise);
        }

        public static void removeEmployeeAt(int index)
        {
            if (index < 0 || index >= employeeList.Count)
                return;
            employeeList.RemoveAt(index);
        }

        

        public static void removeEnterpriseAt(int index)
        {
            if (index < 0 || index >= enterpriseList.Count)
                return;
            enterpriseList.RemoveAt(index);
        }
        public static void SerializeData(string filePath)
        {
            var settings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto, // Сохраняем информацию о типах объектов
                Converters = new List<JsonConverter> { new Newtonsoft.Json.Converters.StringEnumConverter() } // Добавляем конвертер для перечислений, если используются
            };

            var data = new
            {
                EmployeeList = employeeList,
                EnterpriseList = enterpriseList,
                CurrentEnterpriseIndex = currentEnterpriseIndex
            };
            string json = JsonConvert.SerializeObject(data, settings);
            File.WriteAllText(filePath, json);
        }
        public static void DeserializeData(string filePath)
        {
            if (File.Exists(filePath))
            {
                string json = File.ReadAllText(filePath);

                var settings = new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Auto
                };

                var jsonObject = JObject.Parse(json);
                employeeList = JsonConvert.DeserializeObject<List<Employee>>(jsonObject["EmployeeList"].ToString(), settings);
                enterpriseList = JsonConvert.DeserializeObject<List<Enterprise>>(jsonObject["EnterpriseList"].ToString(), settings);
                currentEnterpriseIndex = jsonObject["CurrentEnterpriseIndex"].Value<int>();
            }
        }



    }
}
